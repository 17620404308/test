import copy
'''
反转，判断是否对称
第一种方法：用切片反转
第二种方法：reversed（）返回可迭代对象，在用list()
第三种方法：深拷贝
'''
x = [1, "a",  0, "2", 0, "a", 1]
# 第一种方法：[::-1]
print(x==x[::-1])
# 第二种方法：reversed（）
x2 = reversed(x)#返回可迭代对象，一串二进制码
print(x == list(x2))
b = copy.deepcopy(x)
x.reverse()
print(x==b)
'''
如果有一个列表a=[1,3,5,7,11]
问题：
1如何让它反转成[11,7,5,3,1]
2.取到奇数位值的数字，如[1,5,11]
'''
a=[1,3,5,7,11]
print(a[::2])
'''
a.reverse()不会生成新的列表
reversed(a) 会生成新的列表（可迭代对象）
'''
'''
a = [1, 6, 8, 11, 9, 1, 8, 6, 8, 7, 8] 
从小到大排序
python的排序有两个方法，一个是list对象的sort方法，另外一个是builtin函数里面sorted，主要区别：
sort仅针对于list对象排序，无返回值, 会改变原来队列顺序
sorted是一个单独函数，可以对可迭代（iteration）对象排序，不局限于list，它不改变原生数据，重新生成一个新的队列
    
'''
a = [1, 6, 8, 11, 9, 1, 8, 6, 8, 7, 8]
print(list(sorted(a)))
a.sort()
print(a)
'''
取出最大值最小值
L1 = [1, 2, 3, 11, 2, 5, 3, 2, 5, 33, 88]
找出列表中最大值和最小值
'''
L1 = [1, 2, 3, 11, 2, 5, 3, 2, 5, 33, 88]
print(min(L1))
print(max(L1))
'''
找出列表中单词最长的一个
a = ["hello", "world", "yoyo", "congratulations"]
找出列表中单词最长的一个
'''
a = ["hello", "world", "yoyo", "congratulations"]
# a_len=[]
# for i in a:
#     a_len.append(len(i))
# print(max(a_len))
print(min(a, key=lambda x:len(x)))